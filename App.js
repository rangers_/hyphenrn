/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Stack navigator for landing and signing then stack navigator to contain the home tabs
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, StatusBar} from 'react-native';
import {LandingView, SigningView, AppContainer, SideMenuView} from "./src/views";
import SideMenu from 'react-native-side-menu';
import {Colors, TOGGLER} from './src/config';
import PubSub from 'pubsub-js'


type Props = {};
export default class App extends Component<Props> {
    token: any;

    constructor(props) {
        super(props);

        this.state = {
            loggedIn: false,
            landing: true,
            isOpen: false
        };

        this.unLand = this.unLand.bind(this);
        this.logIn = this.logIn.bind(this);
        // this.toggle = this.toggle.bind(this);
    }

    componentDidMount() {
        this.token = PubSub.subscribe(TOGGLER, this.toggle);
    }

    unLand() {
        this.setState({landing: false})
    }

    logIn() {
        this.setState({loggedIn: true})
    }

    toggle = () => {
        console.log('right here babyyyy..');
        this.setState({isOpen: !this.state.isOpen});
    };

    render() {
        let menu = <SideMenuView/>;
        let RootStack = this.state.loggedIn ?
            <SideMenu menu={menu}
                      isOpen={this.state.isOpen}>
                <AppContainer />
            </SideMenu> : (this.state.landing ? <LandingView unland={this.unLand}/> :
                <SigningView login={this.logIn}/>);

        return (
            <View style={{flex: 1}}>
                <StatusBar
                backgroundColor={Colors.primary}
                barStyle="dark-content"
                />
                {RootStack}
            </View>
        );
    }
}
