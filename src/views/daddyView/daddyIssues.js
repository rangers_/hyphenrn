import React, {Component} from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import {Colors} from '../../config';
import {Text, TouchableNativeFeedback, View} from "react-native";

import {HomeTabs} from "../homeTabsView";
import EditProfilePersonalView from "../editProfilePersonalView/editProfilePersonalView";
import EditProfileBuisnessView from "../editProfileBusinessView/editProfileBuisnessView";
import AboutUsView from "../aboutUsView/aboutUsView";
import HelpView from "../helpView/helpView";
import NavigationService from "../../services/navigation/navigationService";
import {PublicProfileBuisnView} from "../publicProfileBusinView";
import {PublicProfilePersoView} from "../publicProfilePersoView";
import {ChooseCommunityView} from "../chooseCommunityView";
import {AllReviewsView} from "../allReviewsView";


const MainStack = createStackNavigator(
    {
        homeTabs: HomeTabs,
        editPersonal: EditProfilePersonalView,
        editBusiness: EditProfileBuisnessView,
        aboutUs: AboutUsView,
        helpView: HelpView,
        persoView: PublicProfilePersoView,
        businessView: PublicProfileBuisnView,
        chooseCommunity: ChooseCommunityView,
        allReviews: AllReviewsView
    }, {
        initialRouteName: 'homeTabs',
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: Colors.primary,
                elevation: 0,       //remove shadow on Android
                shadowColor: 'transparent',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
    }
);

const AppContainerStack = createAppContainer(MainStack);

export default class DaddyIssue extends Component {
    render() {
        return <AppContainerStack  ref={navigatorRef => {
            NavigationService.setNavigator(navigatorRef, NavigationService.navigators.daddy.name);
        }}/>;
    }
}