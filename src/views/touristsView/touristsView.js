import React, {Component} from 'react';
import {Text, View, ImageBackground, TouchableNativeFeedback, ScrollView} from 'react-native';
import touristsStyleSheet from './touristsStyleSheet';
import Icon from "react-native-vector-icons/MaterialIcons";
import {TouristsItem} from "../../components/touristsItem";
import NavigationService from "../../services/navigation/navigationService";

const daddyNavigator = NavigationService.navigators.daddy;

export default class TouristsView extends Component {

    tourists = [
        {
            name: 'Saiida Sousou',
            image: require('../../../assets/personalProfileImg.jpg'),
            bio: 'Take it easy baby',
            location: 'Hunululu, Street n°95'
        },{
            name: 'Saiida Sousou',
            image: require('../../../assets/personalProfileImg.jpg'),
            bio: 'Take it easy baby',
            location: 'Hunululu, Street n°95'
        },{
            name: 'Saiida Sousou',
            image: require('../../../assets/slide1.jpg'),
            bio: 'Take it easy baby',
            location: 'Hunululu, Street n°95'
        },{
            name: 'Saiida Sousou',
            image: require('../../../assets/slide3.jpg'),
            bio: 'Take it easy baby',
            location: 'Hunululu, Street n°95'
        },
    ];

    static navigationOptions = {
        // headerTitle instead of title
        tabBarIcon: ({focused, tintColor}) =>
            <View>
                <Text
                    style={{
                        fontSize: 20,
                        color: tintColor
                    }}>
                    <Icon name="people" size={35} color={tintColor}/>
                </Text>
            </View>
    };


    goToProfile() {
        NavigationService.navigate(daddyNavigator.name, 'persoView');
    }

    render() {
        return (
            <View style={touristsStyleSheet.ViewStyle}>
                <ScrollView>
                    {
                        this.tourists.map((content, key) =>
                            <TouchableNativeFeedback key={key}
                                                     onPress={this.goToProfile}
                                                     background={TouchableNativeFeedback.SelectableBackground()}>
                                <View>
                                    <TouristsItem name={content.name}
                                                 bio={content.bio}
                                                 location={content.location}
                                                 image={content.image}
                                    />
                                </View>
                            </TouchableNativeFeedback>)
                    }
                </ScrollView>
            </View>
        );
    }
}