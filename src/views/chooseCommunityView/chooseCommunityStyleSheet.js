import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

//dummy styles down there to change
const chooseCommunityStyleSheet = StyleSheet.create({
    ViewStyle: {
        flex: 1,
        height: 100 * GlobalSheet.units.vh,
        width: 100 * GlobalSheet.units.vw
    },
    gridItems: {
        flex: 1
    },
    title: {
        color: Colors.secondary,
        fontWeight: 'bold',
        fontSize: GlobalSheet.widthPercentageToDP(6)
    }
});

export default chooseCommunityStyleSheet;