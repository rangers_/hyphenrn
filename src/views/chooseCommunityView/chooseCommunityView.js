import React, {Component} from 'react';
import {Text, View, TouchableNativeFeedback, ScrollView, TouchableOpacity, AsyncStorage} from 'react-native';
import chooseCommunityStyleSheet from "./chooseCommunityStyleSheet";
import {COMMUNITIES, Colors} from '../../config';
import {ChooseCommunityItem} from "../../components";
import Icon from "react-native-vector-icons/MaterialIcons";
import {NavigationService} from '../../services';
import PubSub from 'pubsub-js'
import {UPDATESUBS} from "../../config";

const daddy = NavigationService.navigators.daddy;

const left = (<Icon name="arrow-back" size={35} color={Colors.secondary}/>);

export class ChooseCommunityView extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (
                <TouchableNativeFeedback
                    onPress={navigation.getParam('goBack')}
                    background={TouchableNativeFeedback.SelectableBackground()}
                >
                    <View style={{marginLeft: 9}}>
                        <Text style={{textAlign: 'center', justifyContent: 'center'}}>{left}</Text>
                    </View>
                </TouchableNativeFeedback>
            ),
            headerTitle: <Text style={chooseCommunityStyleSheet.title}>Select your interest</Text>
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({goBack: this.goBackBtn});
        console.log(NavigationService.getNavParams(daddy.name));
    }

    goBackBtn = () => {
        PubSub.publish(UPDATESUBS, {selectedItems: this.state.selectedItems});
        this.props.navigation.goBack();
    };

    communitiesObjsArray;
    constructor(props) {
        super(props);
        this.communitiesObjsArray = Object.keys(COMMUNITIES);

        this.state = {array: this.communitiesObjsArray, selectedItems: [], ...COMMUNITIES};
    }

    itemClick(item) {
        let obj = {};
        let selectedItems: Array = this.state.selectedItems;
        let index = selectedItems.findIndex(x => {return x === item});

        if (index === -1)  selectedItems.push(item);
        else selectedItems.splice(index, 1);

        obj[item] = Object.assign({}, this.state[item], {selected: !this.state[item].selected});
        this.setState({selectedItems: selectedItems, ...obj});
    }

    render() {
        return (
            <View style={chooseCommunityStyleSheet.ViewStyle}>
                <ScrollView contentContainerStyle={{flexGrow: 1, justifyContent: 'space-between', paddingBottom: 50}}>
                    {
                        this.state.array.map((content, key) =>
                            <View style={chooseCommunityStyleSheet.gridItems}
                                  key={key}>
                                <TouchableOpacity onPress={() => this.itemClick(content)}>
                                    <ChooseCommunityItem
                                        index={key}
                                        image={this.state[content].image}
                                        name={this.state[content].name}
                                        selected={this.state[content].selected}
                                    />
                                </TouchableOpacity>
                            </View>)
                    }
                </ScrollView>
            </View>
        );
    }
}
