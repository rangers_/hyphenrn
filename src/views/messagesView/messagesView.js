import React, {Component} from 'react';
import {Text, View, ImageBackground, TouchableNativeFeedback, ScrollView} from 'react-native';
import messagesStyleSheet from './messagesStyleSheet';
import Icon from "react-native-vector-icons/MaterialIcons";
import {MessageItem} from "../../components";
import {NotificationModel} from "../../models";
import {NOTIFHANDLER} from '../../config';
import PubSub from 'pubsub-js';


export default class MessagesView extends Component {
    messages = [
        {
            name: 'Mouldy Fraj',
            image: require('../../../assets/slide.jpg'),
            message: 'Coco bebee :* !',
            new: false,
            time: 'just now'
        }, {
            name: 'Mouldy Fraj',
            image: require('../../../assets/slide.jpg'),
            message: 'Coco bebee :* !',
            new: false,
            time: 'just now'
        }, {
            name: 'Mouldy Fraj',
            image: require('../../../assets/slide.jpg'),
            message: 'Coco bebee :* !',
            new: false,
            time: 'just now'
        }, {
            name: 'Mouldy Fraj',
            image: require('../../../assets/signin.jpg'),
            message: 'Coco bebee :* !',
            new: false,
            time: 'just now'
        }, {
            name: 'Mouldy Fraj',
            image: require('../../../assets/personalProfileImg.jpg'),
            message: 'Coco bebee :* !',
            new: false,
            time: 'just now'
        }
    ];

    static navigationOptions = {
        tabBarIcon: ({focused, tintColor}) =>
            <View>
                <Text
                    style={{
                        fontSize: 20,
                        color: tintColor
                    }}>
                    <Icon name="message" size={35} color={tintColor}/>
                </Text>
            </View>
    };
    openConversation() {
    };

    openSmallMenu = () => {
        this.setState({
            modalVisibilty: !this.state.modalVisibilty
        });
    };

    constructor(props) {
        super(props);

        this.state = {
            modalVisibilty: false
        }
    }

    componentDidMount() {
        this.token = PubSub.subscribe(NOTIFHANDLER, this.openSmallMenu)
    }


    render() {
        return (
            <View style={messagesStyleSheet.ViewStyle}>
                <NotificationModel modalVisibilty={this.state.modalVisibilty} updateVisibilty={this.openSmallMenu}/>
                <ScrollView>
                    {
                        this.messages.map((content, key) =>
                            <TouchableNativeFeedback key={key}
                                                     onPress={this.openConversation}
                                                     background={TouchableNativeFeedback.SelectableBackground()}>
                                <View>
                                    <MessageItem new={content.new}
                                                 name={content.name}
                                                 message={content.message}
                                                 time={content.time}
                                                 image={content.image}
                                    />
                                </View>
                            </TouchableNativeFeedback>)
                    }
                </ScrollView>
            </View>
        );
    }
}