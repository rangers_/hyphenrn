import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

//dummy styles down there to change
const communitiesListStyleSheet = StyleSheet.create({
    ViewStyle: {
        fontSize: 4 * GlobalSheet.units.vw,
        height: '100%',
        width: '100%'
    },
    bioView: {
        paddingLeft: 50,
        paddingRight: 50,
        paddingTop: 40,
        flex: 1,
        color: Colors.greyOutline,
        width: '100%',
        fontSize: 50,
        textAlign: 'center',
        marginTop: 75,
        fontWeight: 'bold',
        // top: '-5%'
    },
    BtnHolder: {
        borderRadius: 15,
        backgroundColor: Colors.secondary,
        width: 215,
        height: 60,
        marginLeft: 100,
        marginBottom: 100
    },
    BtnText: {
        fontSize: 25,
        fontWeight: 'bold',
        color: Colors.primary,
        textAlign: 'center',
        // marginLeft: 130,
        marginTop: 10,
        // width:'100%'
    }, buttonsContainer: {
        width: GlobalSheet.widthPercentageToDP(100),
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: GlobalSheet.heightPercentageToDP(10)
    },
    SwitchHolder: {
        borderRadius: 15,
        backgroundColor: Colors.secondary,
        width: GlobalSheet.widthPercentageToDP(70),
        height: GlobalSheet.heightPercentageToDP(8),
        opacity: 0.9,
        marginBottom: GlobalSheet.heightPercentageToDP(9),
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    SwitchText: {
        color: '#fff',
        fontSize: GlobalSheet.widthPercentageToDP(5),
        textAlign: 'center',
        fontWeight: 'bold',
        // marginTop:3
    }
});

export default communitiesListStyleSheet;