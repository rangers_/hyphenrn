import React, {Component} from 'react';
import {Text, View, ImageBackground, TouchableNativeFeedback, ScrollView} from 'react-native';
import communitiesListStyleSheet from "./communitiesListStyleSheet";
import {COMMUNITIES} from '../../config';
import {CommunitiesListItem} from "../../components/communitiesListItem";
import {NavigationService} from "../../services";

const daddy = NavigationService.navigators.daddy;

export class CommunitiesListView extends Component {
    communitiesObjesArray;

    constructor(props) {
        super(props);
        this.communitiesObjesArray = this.props.keysArray;
    }

    goToChoose() {
        NavigationService.navigate(daddy.name, 'chooseCommunity', this.communitiesObjesArray)
    }

    render() {
        return (
            <View style={communitiesListStyleSheet.ViewStyle}>
                <ScrollView>
                    {
                        this.communitiesObjesArray.map((content, key) =>

                            <TouchableNativeFeedback onPress={this.goToChoose}
                                                     background={TouchableNativeFeedback.SelectableBackground()}>
                                <View key={key}>
                                    <CommunitiesListItem
                                        image={COMMUNITIES[content].image}
                                        name={COMMUNITIES[content].name}
                                    />
                                </View>
                            </TouchableNativeFeedback>
                        )
                    }
                    <View style={communitiesListStyleSheet.buttonsContainer}>
                        <TouchableNativeFeedback onPress={this.goToChoose}
                                                 background={TouchableNativeFeedback.SelectableBackground()}>
                            <View style={communitiesListStyleSheet.SwitchHolder}>
                                <Text style={communitiesListStyleSheet.SwitchText}>Choose communities</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
