import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback, TouchableOpacity} from 'react-native';
import editProfileBuisnessViewStyleSheet from './editProfileBuisnessViewStyleSheet';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Colors} from '../../config';
import chooseCommunityStyleSheet from "../chooseCommunityView/chooseCommunityStyleSheet";

const personalImage = require("../../../assets/personalProfileImg.jpg");
const camera = (<Icon name="camera-alt" size={35} color={Colors.secondary}/>);
const edit = (<Icon name="edit" size={35} color={Colors.secondary}/>);
const place = (<Icon name="place" size={35} color={Colors.secondary}/>)
const left = (<Icon name="arrow-back" size={35} color={Colors.secondary}/>);

export default class EditProfileBuisnessView extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (
                <TouchableNativeFeedback
                    onPress={navigation.getParam('goBack')}
                    background={TouchableNativeFeedback.SelectableBackground()}
                >
                    <View style={{marginLeft: 9}}>
                        <Text style={{textAlign: 'center', justifyContent: 'center'}}>{left}</Text>
                    </View>
                </TouchableNativeFeedback>
            ),
            headerTitle: <Text style={chooseCommunityStyleSheet.title}>Edit your profile</Text>
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({goBack: this.goBackBtn});
    }

    goBackBtn = () => {
        this.props.navigation.goBack()
    };

    CameraIcon() {

    }

    render() {
        return (
            <View style={editProfileBuisnessViewStyleSheet.ViewStyle}>
                <Image source={personalImage} style={editProfileBuisnessViewStyleSheet.PersonalImage}></Image>
                <TouchableOpacity onPress={this.CameraIcon()}><View
                    style={editProfileBuisnessViewStyleSheet.CameraSyle}><Text>{camera}</Text></View></TouchableOpacity>
                <TouchableOpacity onPress={this.CameraIcon()}><View><Text
                    style={editProfileBuisnessViewStyleSheet.EditStyle2}>{edit}</Text></View></TouchableOpacity>
                <Text style={editProfileBuisnessViewStyleSheet.NameStyle}>Restaurant</Text>
                <TouchableOpacity onPress={this.CameraIcon()}><View><Text
                    style={editProfileBuisnessViewStyleSheet.EditStyle}>{edit}</Text></View></TouchableOpacity>
                <Text style={editProfileBuisnessViewStyleSheet.OriginStyle1}>Description : </Text>
                <View style={editProfileBuisnessViewStyleSheet.editshadow}>
                    <Text style={editProfileBuisnessViewStyleSheet.BioStyle}>This is some dummy bio which should be
                        replaced so please replace me senpai
                    </Text>
                </View><TouchableOpacity onPress={this.CameraIcon()}><View><Text
                style={editProfileBuisnessViewStyleSheet.EditStyle1}>{edit}</Text></View></TouchableOpacity>
                <Text style={editProfileBuisnessViewStyleSheet.OriginStyle}>Location : </Text>
                <View style={editProfileBuisnessViewStyleSheet.editshadow1}>
                    <Text style={editProfileBuisnessViewStyleSheet.EditStyle3}>{place}</Text>
                    <Text style={editProfileBuisnessViewStyleSheet.BioStyle1}>Tunisia
                    </Text>
                </View>
                <View>
                    <TouchableNativeFeedback onPress={this.CameraIcon()}
                                             background={TouchableNativeFeedback.SelectableBackground()}>
                        <View style={editProfileBuisnessViewStyleSheet.SwitchHolder}><Text
                            style={editProfileBuisnessViewStyleSheet.SwitchText}>Switch Back to username</Text>
                        </View>
                    </TouchableNativeFeedback>
                </View>
            </View>
        );


    }
}