import React, {Component} from 'react';
import {Text, View, ImageBackground, TouchableNativeFeedback, ScrollView} from 'react-native';
import BusinessStyleSheet from './businessStyleSheet';

import Icon from 'react-native-vector-icons/MaterialIcons';
import {BusinessItem} from "../../components";
import {NavigationService} from '../../services';

const daddyNavigator = NavigationService.navigators.daddy;

export default class BusinessView extends Component {
    businesses = [
        {
            name: 'Fawzi&Co.',
            image: require('../../../assets/slide1.jpg'),
            description: 'We sell stuff',
            location: 'Tunis, street n°9',
            reviews: '996 reviews'
        },{
            name: 'Fawzi&Co.',
            image: require('../../../assets/slide1.jpg'),
            description: 'We sell stuff',
            location: 'Tunis, street n°9',
            reviews: '996 reviews'
        },{
            name: 'Fawzi&Co.',
            image: require('../../../assets/slide1.jpg'),
            description: 'We sell stuff',
            location: 'Tunis, street n°9',
            reviews: '996 reviews'
        },{
            name: 'Fawzi&Co.',
            image: require('../../../assets/slide1.jpg'),
            description: 'We sell stuff',
            location: 'Tunis, street n°9',
            reviews: '996 reviews'
        },{
            name: 'Fawzi&Co.',
            image: require('../../../assets/slide1.jpg'),
            description: 'We sell stuff',
            location: 'Tunis, street n°9',
            reviews: '996 reviews'
        },{
            name: 'Fawzi&Co.',
            image: require('../../../assets/slide1.jpg'),
            description: 'We sell stuff',
            location: 'Tunis, street n°9',
            reviews: '996 reviews'
        },{
            name: 'Fawzi&Co.',
            image: require('../../../assets/slide1.jpg'),
            description: 'We sell stuff',
            location: 'Tunis, street n°9',
            reviews: '996 reviews'
        }
    ];

    goToProfile() {
        NavigationService.navigate(daddyNavigator.name, 'businessView');
    }

    static navigationOptions = {
        // headerTitle instead of title
        tabBarIcon: ({focused, tintColor}) =>
            <View>
                <Text
                    style={{
                        fontSize: 20,
                        color: tintColor
                    }}>
                    <Icon name="store" size={35} color={tintColor}/>
                </Text>
            </View>
    };

    render() {
        return (
            <View style={BusinessStyleSheet.ViewStyle}>
                <ScrollView>
                    {
                        this.businesses.map((content, key) =>
                            <TouchableNativeFeedback key={key}
                                                     onPress={this.goToProfile}
                                                     background={TouchableNativeFeedback.SelectableBackground()}>
                                <View>
                                    <BusinessItem name={content.name}
                                                  description={content.bio}
                                                  location={content.location}
                                                  image={content.image}
                                                  reviews={content.reviews}
                                    />
                                </View>
                            </TouchableNativeFeedback>)
                    }
                </ScrollView>
            </View>
        );
    }
}