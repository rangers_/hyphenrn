import React, {Component} from 'react';
import {Text, View, ImageBackground, TouchableNativeFeedback} from 'react-native';
import commnitiesStyleSheet from './communitiesViewStyleSheet';
import Icon from "react-native-vector-icons/MaterialIcons";
import {CommunitiesEmptyView} from "../communitiesEmptyView";
import {CommunitiesListView} from "../communitiesListView";
import {NavigationService} from '../../services';
import PubSub from 'pubsub-js'
import {UPDATESUBS} from "../../config";

const daddyNavigator = NavigationService.navigators.daddy;

export default class CommunitiesView extends Component {
    static navigationOptions = {
        tabBarIcon: ({focused, tintColor}) =>
            <View>
                <Text
                    style={{
                        fontSize: 20,
                        color: tintColor
                    }}>
                    <Icon name="favorite" size={35} color={tintColor}/>
                </Text>
            </View>
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedItems: []
        }
    }

    tokenSub;
    componentDidMount() {
        this.tokenSub = PubSub.subscribe(UPDATESUBS, this.updateList);

    }

    updateList = (msg, data) => {
        this.setState(data);
    };

    goToJoinComms() {
        NavigationService.navigate(daddyNavigator.name, 'chooseCommunity');
    }

    render() {
        return (
            <View style={commnitiesStyleSheet.ViewStyle}>
                {
                    (this.state.selectedItems.length === 0) ? <CommunitiesEmptyView joinClicked={this.goToJoinComms}/> :
                        <CommunitiesListView keysArray={this.state.selectedItems}/>
                }
            </View>
        );
    }
}