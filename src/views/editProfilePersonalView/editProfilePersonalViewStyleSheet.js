import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

const editProfilePersonalViewStyleSheet = StyleSheet.create({
    ViewStyle: {
        flex: 1,
        fontSize: 4 * GlobalSheet.units.vw,
        backgroundColor: Colors.primary1
    },
    ProfileSettings:{
        fontWeight:'bold',
        marginLeft: 20,
        fontSize:25,
        marginTop:10
    },
    ProfileSettings1:{
        fontWeight:'bold',
        marginLeft: 20,
        fontSize:25,
        marginTop:20
    },
    PersonalImage: {
        width: 50 * GlobalSheet.units.vw,
        height: 50 * GlobalSheet.units.vw,
        borderRadius: 50*GlobalSheet.units.vw,
        marginLeft: 100,
        marginTop: 10,
        borderColor:Colors.secondary,
        borderWidth:3
    },
    ImageView: {
        width: 200,
        height: 200,
        borderRadius: 100
    },
    NameStyle:{
        fontSize: 35,
        color: Colors.grey3,
        fontWeight: 'bold',
        marginLeft: 110,
        marginTop: 0
    },
    BioStyle:{
        fontSize: 20,
        color: '#000',
        //fontWeight: 'bold',
        marginLeft: 30,
        marginTop: 10,
        marginBottom: 20,
        backgroundColor: 'white',
    },
    CameraSyle:{
        marginLeft:270,
        marginTop:-30
    },
    editshadow:{
        marginTop:20,
        width:'110%',
        marginLeft:-10,
        borderWidth: 1,
        borderRadius: 2,
        borderColor: 'white',
        backgroundColor: Colors.primary,
        borderBottomWidth: 0,
        elevation: 5,
        shadowOpacity: 0.8
    },
    editshadow1:{
        marginTop:40,
        width:'110%',
        marginLeft:-10,
        borderWidth: 1,
        borderRadius: 2,
        borderColor: 'white',
        backgroundColor: Colors.primary,
        borderBottomWidth: 0,
        elevation: 5,
        shadowOpacity: 0.8,
        marginBottom:-5
    },
    OriginStyle:{
        fontWeight:'bold',
      marginLeft: 20,
      marginTop :-27,
      marginBottom:-40,
      fontSize:25
    },
    OriginStyle1:{
        fontWeight:'bold',
        marginLeft: 20,
        marginTop :15,
        marginBottom:-20,
        fontSize:25
    },
    EditStyle:{
        marginLeft:75,
        marginTop:10,
        marginBottom:-45
    },
    EditStyle1:{
        marginLeft:110,
        marginTop:20,
        marginBottom:-7
    },
    EditStyle2:{
        marginLeft:320,
        marginTop:10,
        marginBottom:-40
    },
    BioStyle1:{
        fontSize: 20,
        color: '#000',
        marginLeft: 60,
        marginTop: -30,
        marginBottom: 20
    },
    EditStyle3:{
        marginLeft:20
    },
    SwitchHolder: {
        borderRadius: 15,
        backgroundColor: Colors.secondary,
        width: 225,
        height: 60,
        marginLeft: 95,
        marginBottom: 30,
        marginTop:30
    },
    SwitchText:{
        color: '#fff',
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop:15
    }
});
export default editProfilePersonalViewStyleSheet;