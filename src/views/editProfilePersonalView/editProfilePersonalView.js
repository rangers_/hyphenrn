import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback, ScrollView} from 'react-native';
import editProfilePersonalViewStyleSheet from './editProfilePersonalViewStyleSheet';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Colors} from '../../config';
import ToggleSwitch from 'toggle-switch-react-native';
import chooseCommunityStyleSheet from "../chooseCommunityView/chooseCommunityStyleSheet";


const personalImage = require("../../../assets/personalProfileImg.jpg");
const camera = (<Icon name="camera-alt" size={35} color={Colors.secondary}/>);
const edit = (<Icon name="edit" size={35} color={Colors.secondary}/>);
const place = (<Icon name="place" size={35} color={Colors.secondary}/>);
const left = (<Icon name="arrow-back" size={35} color={Colors.secondary}/>);

export default class EditProfilePersonalView extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (
                <TouchableNativeFeedback
                    onPress={navigation.getParam('goBack')}
                    background={TouchableNativeFeedback.SelectableBackground()}
                >
                    <View style={{marginLeft: 9}}>
                        <Text style={{textAlign: 'center', justifyContent: 'center'}}>{left}</Text>
                    </View>
                </TouchableNativeFeedback>
            ),
            headerTitle: <Text style={chooseCommunityStyleSheet.title}>Edit your profile</Text>
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({goBack: this.goBackBtn});
    }

    goBackBtn = () => {
        this.props.navigation.goBack()
    };

    CameraIcon() {

    }

    render() {
        return (
            <ScrollView>
                <View style={editProfilePersonalViewStyleSheet.ViewStyle}>
                    <Image source={personalImage} style={editProfilePersonalViewStyleSheet.PersonalImage}></Image>
                    <View><Text style={editProfilePersonalViewStyleSheet.CameraSyle}>{camera}</Text></View>
                    <Text style={editProfilePersonalViewStyleSheet.EditStyle2}>{edit}</Text>
                    <Text style={editProfilePersonalViewStyleSheet.NameStyle}>Ahmed Sbai</Text>
                    <Text style={editProfilePersonalViewStyleSheet.ProfileSettings}>Profile Settings : </Text>
                    <Text style={editProfilePersonalViewStyleSheet.EditStyle}>{edit}</Text>
                    <Text style={editProfilePersonalViewStyleSheet.OriginStyle1}>Bio : </Text>
                    <View style={editProfilePersonalViewStyleSheet.editshadow}>
                        <Text style={editProfilePersonalViewStyleSheet.BioStyle}>This is some dummy bio which should be
                            replaced so please replace me senpai
                        </Text>
                    </View>
                    <Text style={editProfilePersonalViewStyleSheet.EditStyle1}>{edit}</Text>
                    <Text style={editProfilePersonalViewStyleSheet.OriginStyle}>Origin : </Text>
                    <View style={editProfilePersonalViewStyleSheet.editshadow1}>
                        <Text style={editProfilePersonalViewStyleSheet.EditStyle3}>{place}</Text>
                        <Text style={editProfilePersonalViewStyleSheet.BioStyle1}>Tunisia
                        </Text>
                    </View>
                    <Text style={editProfilePersonalViewStyleSheet.ProfileSettings1}>Notification Settings : </Text>
                    <ToggleSwitch
                        isOn={true}
                        onColor='#fc8966'
                        offColor='white'
                        label='Notifications'
                        labelStyle={{color: 'black', fontSize:20, marginLeft:20, marginTop:10}}
                        size='large'
                        onToggle={(isOn) => console.log('changed to : ', isOn)}
                    /><View>
                    <TouchableNativeFeedback onPress={this.CameraIcon()}
                                             background={TouchableNativeFeedback.SelectableBackground()}>
                        <View style={editProfilePersonalViewStyleSheet.SwitchHolder}><Text
                            style={editProfilePersonalViewStyleSheet.SwitchText}>Switch To Buisness</Text>
                        </View>
                    </TouchableNativeFeedback>
                </View>
                </View>
            </ScrollView>
        );


    }
}