import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback, TouchableOpacity, ScrollView} from 'react-native';
import PublicProfilePersoViewStyleSheet from './PublicProfilePersoViewStyleSheet';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Colors} from '../../config';
import chooseCommunityStyleSheet from "../chooseCommunityView/chooseCommunityStyleSheet";

const personalImage = require("../../../assets/personalProfileImg.jpg");
const left = (<Icon name="arrow-back" size={35} color={Colors.secondary}/>);
const place = (<Icon name="place" size={30} color={Colors.secondary}/>);

export default class PublicProfilePersoView extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (
                <TouchableNativeFeedback
                    onPress={navigation.getParam('goBack')}
                    background={TouchableNativeFeedback.SelectableBackground()}
                >
                    <View style={{marginLeft: 9}}>
                        <Text style={{textAlign: 'center', justifyContent: 'center'}}>{left}</Text>
                    </View>
                </TouchableNativeFeedback>
            ),
            headerTitle: <Text style={chooseCommunityStyleSheet.title}>Mohsen hosni</Text>
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({goBack: this.goBackBtn});
    }

    goBackBtn = () => {
        this.props.navigation.goBack()
    };

    CameraIcon() {

    }

    render() {
        return (
            <View style={PublicProfilePersoViewStyleSheet.ViewStyle}>
                <ScrollView>
                    <Image source={personalImage} style={PublicProfilePersoViewStyleSheet.PersonalImage}/>
                    <View style={PublicProfilePersoViewStyleSheet.buttonsContainer}>
                        <Text style={PublicProfilePersoViewStyleSheet.NameStyle}>Sousou somayya</Text>
                    </View>

                    <View style={PublicProfilePersoViewStyleSheet.buttonsContainer}>
                        <TouchableNativeFeedback onPress={this.CameraIcon()}
                                                 background={TouchableNativeFeedback.SelectableBackground()}>
                            <View style={PublicProfilePersoViewStyleSheet.SwitchHolder}>
                                <Text style={PublicProfilePersoViewStyleSheet.SwitchText}>Message</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View>

                    <View style={PublicProfilePersoViewStyleSheet.contentContainer}>
                        <Text style={PublicProfilePersoViewStyleSheet.OriginStyle}>Bio: </Text>
                        <Text style={PublicProfilePersoViewStyleSheet.textStyle}>This is some dummy bio which should
                            be
                            replaced so please replace me senpai
                        </Text>
                    </View>

                    <View style={PublicProfilePersoViewStyleSheet.contentContainer}>
                        <Text style={PublicProfilePersoViewStyleSheet.OriginStyle}>Origin: </Text>
                        <Text style={PublicProfilePersoViewStyleSheet.textStyle}>
                            <Text>{place}</Text>
                            Tunisia
                        </Text>
                    </View>
                </ScrollView>
            </View>
        );


    }
}