import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

//dummy styles down there to change
const singningStyleSheet = StyleSheet.create({
    ViewStyle: {
        fontSize: 4 * GlobalSheet.units.vw,
        height: '100%',
        width: '100%'
    },
    bioView: {
        flex: 1,
        color: '#fff',
        width: '100%',
        fontSize: 80,
        textAlign: 'center',
        marginTop: 75,
        fontWeight: 'bold',
        // top: '-5%'
    },
    bgimage: {
        height: '100%',
        width: '100%',
        //marginRight: '70%'
    },
    secondHeader: {
        color: 'white',
        width: '100%',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 60,
        marginBottom: 50
    },
    fbBtnText: {
        color: '#fff',
        fontSize: 27,
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: 10
    },
    GglBtnText: {
        color: '#fff',
        fontSize: 27,
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: 10
    },
    fbBtnHolder: {
        borderRadius: 15,
        backgroundColor: Colors.facebook,
        width: 225,
        height: 60,
        marginLeft: 100,
        marginBottom: 30
    },
    GglBtnHolder: {
        borderRadius: 15,
        backgroundColor: Colors["google-plus-official"],
        width: 225,
        height: 60,
        marginLeft: 100,
        marginBottom: 100
    }
});

export default singningStyleSheet;