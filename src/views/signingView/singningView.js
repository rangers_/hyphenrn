import React, {Component} from 'react';
import {Text, View, ImageBackground, TouchableNativeFeedback} from 'react-native';
import singningStyleSheet from './singningStyleSheet';
import PropTypes from "prop-types";

const signin = require("../../../assets/signin.jpg");


export class SigningView extends Component {
    facebook() {
    }

    render() {
        return (
            <View style={singningStyleSheet.ViewStyle}>
                <ImageBackground source={signin} style={singningStyleSheet.bgimage}>
                    <Text style={singningStyleSheet.bioView}>Hyphen</Text>
                    <Text style={singningStyleSheet.secondHeader}>Start Your Experience</Text>
                    <TouchableNativeFeedback onPress={this.props.login}
                                             background={TouchableNativeFeedback.SelectableBackground()}>
                        <View style={singningStyleSheet.fbBtnHolder}><Text
                            style={singningStyleSheet.fbBtnText}>FaceBook</Text></View></TouchableNativeFeedback>
                    <TouchableNativeFeedback onPress={this.props.login}
                                             background={TouchableNativeFeedback.SelectableBackground()}>
                        <View style={singningStyleSheet.GglBtnHolder}><Text
                            style={singningStyleSheet.GglBtnText}>Google</Text></View></TouchableNativeFeedback>
                </ImageBackground>
            </View>
        );
    }
}
