
import React, {Component} from 'react';
import {Text, View, TouchableOpacity, TouchableNativeFeedback, Image} from 'react-native';
import conversationViewStyleSheet from './conversationViewStyleSheet';
import {NavigationService} from '../../services';
import PropTypes from "prop-types";
import GlobalSheet from "../../config/globalStyles";
import Icon from "react-native-vector-icons/MaterialIcons";
import {Colors} from '../../config';
import {GiftedChat, Actions, Bubble, SystemMessage} from 'react-native-gifted-chat';

const left = (<Icon name="arrow-back" size={35} color={Colors.secondary}/>);


const navigators = NavigationService.navigators;

export default class ConversationView extends Component {
    imageDummy;
    imageStyle;
    constructor(props) {
        super(props);
        if (this.props.height) {
            this.imageStyle = {
                ...conversationViewStyleSheet.ImageStyle,
                height: this.props.height,
                width: this.props.height,
                borderRadius: this.props.height / 2
            }
        } else {
            this.imageStyle = {
                ...conversationViewStyleSheet.ImageStyle,
                height: 150 * GlobalSheet.units.vw,
                width: 150 * GlobalSheet.units.vw,
                borderRadius: (150 * GlobalSheet.units.vw) / 2
            };
        }

        if (this.props.image) this.imageDummy = this.props.image;
        else this.imageDummy = require('../../../assets/about.jpg');
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (
                <TouchableNativeFeedback
                    onPress={navigation.getParam('goBack')}
                    background={TouchableNativeFeedback.SelectableBackground()}
                >
                    <View style={{marginLeft: 9}}>
                        <Text style={{textAlign: 'center', justifyContent: 'center'}}>{left}</Text>
                    </View>
                </TouchableNativeFeedback>
            ),
            headerTitle: <Text style={chooseCommunityStyleSheet.title}>Mouldy Fraj</Text>
        }
    };


    componentDidMount() {
        this.props.navigation.setParams({goBack: this.goBackBtn});
    }

    goBackBtn = () => {
        this.props.navigation.goBack()
    };


    _onPressButton(route) {
    }

    render() {
        return (
            <View style={conversationViewStyleSheet.viewStyle}>
                <Text>Conveerss</Text>
            </View>
        );
    }
}
ConversationView.propTypes = {
    text: PropTypes.string,
    height:PropTypes.number,
    width:PropTypes.number,
};
