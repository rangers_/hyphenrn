import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

//dummy styles down there to change
const conversationViewStyleSheet = StyleSheet.create({
    viewStyle:{
       // alignSelf: 'center',
       backgroundColor: '#f3f3f3',
        zIndex:-0.5,
    },


    txt1Style: {
        fontWeight: 'bold',
        fontSize: 10 * GlobalSheet.units.vw,
        marginTop: 12,
        paddingLeft: 50,
        paddingRight: 50,
        textAlign: 'center',
        color: '#000000' ,



    },
    txtStyle: {
        fontSize: 23,
        zIndex:2,
        fontFamily: 'verdana',
        textAlign: 'center',
        color: '#000000' ,
        paddingLeft: 35,
        marginTop: 30,
        paddingRight: 35,


    },
    ImageStyle: {
        // width: 25 * GlobalSheet.units.vw,
        // height: 25 * GlobalSheet.units.vw,
        // borderRadius: 30 * GlobalSheet.units.vw,
        paddingLeft:-50,
        marginTop:-100 ,
        alignSelf: 'center'
    }
});

export default conversationViewStyleSheet;