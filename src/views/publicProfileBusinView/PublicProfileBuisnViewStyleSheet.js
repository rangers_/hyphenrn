import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

const PublicProfileBuisnViewStyleSheet = StyleSheet.create({
    ViewStyle: {
        flex: 2,
        fontSize: 4 * GlobalSheet.units.vw,
        backgroundColor: Colors.primary1
    },
    PersonalImage: {
        width: 50 * GlobalSheet.units.vw,
        height: 50 * GlobalSheet.units.vw,
        borderRadius: 50 * GlobalSheet.units.vw,
        marginLeft: 100,
        marginTop: 10,
        borderColor: Colors.secondary,
        borderWidth: 3
    },
    ImageView: {
        width: 200,
        height: 200,
        borderRadius: 100
    },
    NameStyle: {
        fontSize: 35,
        color: Colors.grey3,
        fontWeight: 'bold',
        marginTop: 0
    },
    textStyle: {
        fontSize: 20,
        color: '#000',
        width: GlobalSheet.widthPercentageToDP(100),
        // marginLeft: 30,
        marginTop: 20,
        // marginBottom: 20,
        paddingLeft: 30,
        paddingTop: 15,
        paddingBottom: 15,
        backgroundColor: 'white',
        elevation: 6
    },
    reviewsStyle: {
        width: GlobalSheet.widthPercentageToDP(100),
        marginTop: 20,
        paddingLeft: 10,
        paddingTop: 15,
        paddingBottom: 15,
        backgroundColor: 'white',
        elevation: 6
    },
    CameraSyle: {
        marginLeft: 270,
        marginTop: -30,
        width: 35
    },
    editshadow: {
        marginTop: 20,
        width: GlobalSheet.widthPercentageToDP(100),
        // marginLeft:-10,
        borderWidth: 1,
        borderRadius: 2,
        borderColor: 'white',
        backgroundColor: Colors.primary,
        borderBottomWidth: 0,
        elevation: 5,
        shadowOpacity: 0.8
    },
    editLocation: {
        // marginTop:40,
        width: GlobalSheet.widthPercentageToDP(100),
        height: GlobalSheet.heightPercentageToDP(8),
        // marginLeft:-10,
        borderWidth: 1,
        borderRadius: 2,
        borderColor: 'white',
        backgroundColor: Colors.primary,
        borderBottomWidth: 0,
        elevation: 5,
        shadowOpacity: 0.8,
        paddingTop: 5,
        paddingLeft: 30
        // marginBottom:-5
    },
    OriginStyle_faulty: {
        fontWeight: 'bold',
        marginLeft: 20,
        // marginTop: -35,
        // marginBottom: 35,
        fontSize: 25
    },
    OriginStyle: {
        fontWeight: 'bold',
        marginLeft: 20,
        marginTop: 5,
        marginBottom: -15,
        fontSize: 25
    },
    EditStyle: {
        marginLeft: 163,
        marginTop: 0,
        marginBottom: -36
    },
    EditStyle1: {
        marginLeft: 132,
        marginTop: 20,
        marginBottom: 5
    },
    EditStyle2: {
        marginLeft: 320,
        marginTop: 3,
        marginBottom: -50
    },
    BioStyle1: {
        fontSize: 20,
        color: '#000',
        marginLeft: 60,
        marginTop: -30,
        marginBottom: 20
    },
    EditStyle3: {
        marginLeft: 20
    },
    SwitchHolder: {
        borderRadius: 15,
        backgroundColor: Colors.secondary,
        width: GlobalSheet.widthPercentageToDP(40),
        height: GlobalSheet.heightPercentageToDP(8),
        // marginLeft: 95,
        marginBottom: GlobalSheet.heightPercentageToDP(9),
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    SwitchText: {
        color: '#fff',
        fontSize: GlobalSheet.widthPercentageToDP(6),
        textAlign: 'center',
        fontWeight: 'bold',
        // marginTop:3
    }, buttonsContainer: {
        width: GlobalSheet.widthPercentageToDP(100),
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: GlobalSheet.heightPercentageToDP(3)
    }, contentContainer: {
        marginBottom: GlobalSheet.heightPercentageToDP(8)
    }, locationStyle: {
        fontSize: GlobalSheet.widthPercentageToDP(5)
    },
    seeAllStyle: {
        color: Colors.secondary,
        fontWeight: 'bold',
        fontSize: 23
    }
});
export default PublicProfileBuisnViewStyleSheet;