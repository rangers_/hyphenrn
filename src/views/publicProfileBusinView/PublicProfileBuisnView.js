import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback, TouchableOpacity, ScrollView} from 'react-native';
import PublicProfileBuisnViewStyleSheet from './PublicProfileBuisnViewStyleSheet';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Colors} from '../../config';
import {ReviewItem} from "../../components";
import NavigationService from "../../services/navigation/navigationService";
import chooseCommunityStyleSheet from "../chooseCommunityView/chooseCommunityStyleSheet";

const daddy = NavigationService.navigators.daddy;

const personalImage = require("../../../assets/personalProfileImg.jpg");
const left = (<Icon name="arrow-back" size={35} color={Colors.secondary}/>);
const place = (<Icon name="place" size={30} color={Colors.secondary}/>);

export default class PublicProfileBuisnView extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (
                <TouchableNativeFeedback
                    onPress={navigation.getParam('goBack')}
                    background={TouchableNativeFeedback.SelectableBackground()}
                >
                    <View style={{marginLeft: 9}}>
                        <Text style={{textAlign: 'center', justifyContent: 'center'}}>{left}</Text>
                    </View>
                </TouchableNativeFeedback>
            ),
            headerTitle: <Text style={chooseCommunityStyleSheet.title}>Sousou&Hasna Ste</Text>
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            commentShowAll: false
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({goBack: this.goBackBtn});
    }

    goBackBtn = () => {
        this.props.navigation.goBack()
    };

    CameraIcon() {

    }

    navigateToAll() {
        NavigationService.navigate(daddy.name, 'allReviews');
    }

    expandComment() {
        this.setState({
            commentShowAll: !this.state.commentShowAll
        })
    }

    render() {
        return (
            <View style={PublicProfileBuisnViewStyleSheet.ViewStyle}>
                <ScrollView>
                    <Image source={personalImage} style={PublicProfileBuisnViewStyleSheet.PersonalImage}/>
                    <View style={PublicProfileBuisnViewStyleSheet.buttonsContainer}>
                        <Text style={PublicProfileBuisnViewStyleSheet.NameStyle}>Restaurant</Text>
                    </View>
                    <View style={PublicProfileBuisnViewStyleSheet.buttonsContainer}>
                        <TouchableNativeFeedback onPress={this.CameraIcon()}
                                                 background={TouchableNativeFeedback.SelectableBackground()}>
                            <View style={PublicProfileBuisnViewStyleSheet.SwitchHolder}>
                                <Text style={PublicProfileBuisnViewStyleSheet.SwitchText}>Message</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View>

                    <View style={PublicProfileBuisnViewStyleSheet.contentContainer}>
                        <Text style={PublicProfileBuisnViewStyleSheet.OriginStyle}>Description: </Text>
                        <Text style={PublicProfileBuisnViewStyleSheet.textStyle}>This is some dummy bio which should
                            be
                            replaced so please replace me senpai
                        </Text>
                    </View>

                    <View style={PublicProfileBuisnViewStyleSheet.contentContainer}>
                        <Text style={PublicProfileBuisnViewStyleSheet.OriginStyle}>Location: </Text>
                        <Text style={PublicProfileBuisnViewStyleSheet.textStyle}>
                            <Text>{place}</Text>
                            Tunisia
                        </Text>
                    </View>

                    <View style={PublicProfileBuisnViewStyleSheet.contentContainer}>
                        <Text style={PublicProfileBuisnViewStyleSheet.OriginStyle}>Reviews: </Text>
                        <View style={PublicProfileBuisnViewStyleSheet.reviewsStyle}>
                            <TouchableNativeFeedback
                                onPress={() => this.setState({
                                    commentShowAll: !this.state.commentShowAll
                                })}
                                background={TouchableNativeFeedback.SelectableBackground()}>
                                <View>
                                    <ReviewItem commentShowAll={this.state.commentShowAll}/>
                                </View>
                            </TouchableNativeFeedback>
                            <View style={PublicProfileBuisnViewStyleSheet.buttonsContainer}>
                                <TouchableOpacity
                                onPress={this.navigateToAll}>
                                    <Text style={PublicProfileBuisnViewStyleSheet.seeAllStyle}>
                                        See all reviews
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );


    }
}