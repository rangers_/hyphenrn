import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

//dummy styles down there to change
const sideMenuStyleSheet = StyleSheet.create({
    ViewStyle: {
        flex: 1,
        height: '100%',
        width: '100%',
        backgroundColor: Colors.primary1
    },
    buttonsLeft: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    pictNameContainer: {
        marginTop: 4 * GlobalSheet.units.vh,
        marginLeft: 6 * GlobalSheet.units.vw,
    },
    buttonsView: {
        top: 40 * GlobalSheet.units.vh,
        left: 5 * GlobalSheet.units.vw,
        height: 70 * GlobalSheet.units.vh
    },
    spacer: {
        height: 3 * GlobalSheet.units.vh
    }
});

export default sideMenuStyleSheet;