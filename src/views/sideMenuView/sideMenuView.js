import React, {Component} from 'react';
import {Text, View, ScrollView, TouchableNativeFeedback} from 'react-native';
import sideMenuStyleSheet from './sideMenuStyleSheet';
import {SideMenuProfile, SideMenuButtons} from "../../components";
import NavigationService from "../../services/navigation/navigationService";

const navigators = NavigationService.navigators;

export default class SideMenuView extends Component {

    buttons = [
        {
            name: 'Chat',
            route: 'messages'
        }, {
            name: 'Who\'s around?',
            route: 'tourists'
        }, {
            name: 'What\'s around?',
            route: 'business'
        },{
            name: 'Communities',
            route: 'communities'
        }
    ];
    _buttons = [
        {
            name: 'Settings',
            route: 'editPersonal'
        }, {
            name: 'About',
            route: 'aboutUs'
        }, {
            name: 'Help',
            route: 'helpView'
        }
    ];
    logOut = [
        {
            name: 'Log-out',
            route: null
        }
    ];

    render() {
        return (
            <View style={sideMenuStyleSheet.ViewStyle}>
                <View style={sideMenuStyleSheet.pictNameContainer}>
                    <SideMenuProfile/>
                </View>
                <View style={sideMenuStyleSheet.buttonsView}>
                    <ScrollView contentContainerStyle={{flexGrow: 1}}>
                        <SideMenuButtons daddy={navigators.main.name} buttonsList={this.buttons}/>
                        <View style={sideMenuStyleSheet.spacer}/>
                        <SideMenuButtons daddy={navigators.daddy.name} buttonsList={this._buttons}/>
                        <View style={sideMenuStyleSheet.spacer}/>
                        <SideMenuButtons daddy={null} buttonsList={this.logOut}/>
                    </ScrollView>
                </View>
            </View>
        );
    }
}