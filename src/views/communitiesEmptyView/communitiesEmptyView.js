import React, {Component} from 'react';
import {Text, View, ImageBackground, TouchableNativeFeedback} from 'react-native';

import communitiesEmptyStyleSheet from "./communitiesEmptyStyleSheet";


export class CommunitiesEmptyView extends Component {

    render() {
        return (
            <View style={communitiesEmptyStyleSheet.ViewStyle}>
                <Text style={communitiesEmptyStyleSheet.bioView}>You're not part
                    of any community
                </Text>
                <TouchableNativeFeedback onPress={this.props.joinClicked}
                                         background={TouchableNativeFeedback.SelectableBackground()}>
                    <View style={communitiesEmptyStyleSheet.BtnHolder}><Text
                        style={communitiesEmptyStyleSheet.BtnText}>Join now</Text>
                    </View>
                </TouchableNativeFeedback>
            </View>
        );
    }
}
