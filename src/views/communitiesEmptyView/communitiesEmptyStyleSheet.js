import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

//dummy styles down there to change
const communitiesEmptyStyleSheet = StyleSheet.create({
    ViewStyle: {
        fontSize: 4 * GlobalSheet.units.vw,
        height: '100%',
        width: '100%'
    },
    bioView: {
        paddingLeft:50,
        paddingRight:50,
        paddingTop:40,
        flex: 1,
        color:Colors. greyOutline,
        width: '100%',
        fontSize: 50,
        textAlign: 'center',
        marginTop: 75,
        fontWeight: 'bold',
        // top: '-5%'
    },



    BtnHolder: {
        borderRadius: 15,
        backgroundColor: Colors.secondary,
        width: 215,
        height: 60,
        marginLeft: 100,
        marginBottom: 100
    },
    BtnText :{

        // bottom: 0,
        // left: 0,
         fontSize: 25,
        fontWeight: 'bold',
        color: Colors.primary,
        textAlign:'center',
       // marginLeft: 130,
       marginTop: 10,
        // width:'100%'

    }
});

export default communitiesEmptyStyleSheet;