
import React, {Component} from 'react';
import {Text, View, TouchableOpacity, TouchableNativeFeedback, Image} from 'react-native';
import aboutUsStyleSheet from './aboutUsStyleSheet';
import {NavigationService} from '../../services';
import PropTypes from "prop-types";
import GlobalSheet from "../../config/globalStyles";
import Icon from "react-native-vector-icons/MaterialIcons";
import {Colors} from '../../config';
import chooseCommunityStyleSheet from "../chooseCommunityView/chooseCommunityStyleSheet";
const left = (<Icon name="arrow-back" size={35} color={Colors.secondary}/>);


const navigators = NavigationService.navigators;

export default class AboutUsView extends Component {
    imageDummy;
    imageStyle;
    constructor(props) {
        super(props);
        if (this.props.height) {
            this.imageStyle = {
                ...aboutUsStyleSheet.ImageStyle,
                height: this.props.height,
                width: this.props.height,
                borderRadius: this.props.height / 2
            }
        } else {
            this.imageStyle = {
                ...aboutUsStyleSheet.ImageStyle,
                height: 150 * GlobalSheet.units.vw,
                width: 150 * GlobalSheet.units.vw,
                borderRadius: (150 * GlobalSheet.units.vw) / 2
            };
        }

        if (this.props.image) this.imageDummy = this.props.image;
        else this.imageDummy = require('../../../assets/about.jpg');
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (
                <TouchableNativeFeedback
                    onPress={navigation.getParam('goBack')}
                    background={TouchableNativeFeedback.SelectableBackground()}
                >
                    <View style={{marginLeft: 9}}>
                        <Text style={{textAlign: 'center', justifyContent: 'center'}}>{left}</Text>
                    </View>
                </TouchableNativeFeedback>
            ),
            headerTitle: <Text style={chooseCommunityStyleSheet.title}>About us</Text>
        }
    };


    componentDidMount() {
        this.props.navigation.setParams({goBack: this.goBackBtn});
    }

    goBackBtn = () => {
        this.props.navigation.goBack()
    };


    _onPressButton(route) {
    }

    render() {
        return (
            <View style={aboutUsStyleSheet.viewStyle}>
                <Text style={aboutUsStyleSheet.txt1Style}> Hyphen </Text>
                <Text style={aboutUsStyleSheet.txtStyle}>We are Rangers Team a Branch Of Club ROBOTech FSM Which Were Created In 2017
                This app was developed by : Badii Boutar, Olfa Bouzidi, Ahmed Sbai </Text>
                <Image source={this.imageDummy} style={this.imageStyle}/>
            </View>
        );
    }
}
AboutUsView.propTypes = {
    text: PropTypes.string,
    height:PropTypes.number,
    width:PropTypes.number,
};
