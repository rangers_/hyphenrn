import React, {Component} from 'react';
import {Text, View, Dimensions, TouchableNativeFeedback} from 'react-native';
import Swiper from 'react-native-swiper';
import landingStyleSheet from './landingStyleSheet';
import {CarouselSlide} from "../../components";
import PropTypes from "prop-types";


const CarouselWidth = Dimensions.get('window').width;
const CarouselHeight = Dimensions.get('window').height;

export class LandingView extends Component {

    constructor(props) {
        super(props);
    }

    images = [
        {
            link: require('../../../assets/slide1.jpg'),
            text: 'Chat with all the tourists in your same destination!'
        },
        {
            link: require('../../../assets/slide3.jpg'),
            text: 'Find communities in the same interest you desire!'
        },
        {
            link: require('../../../assets/slide2.jpg'),
            text: 'Hyphen, will make your experience more authentic!'
        }
    ];

    render() {
        return (
            <View style={landingStyleSheet.view}>
                <Swiper loop={true} activeDotColor={'#fff'} dotStyle={landingStyleSheet.dots}
                        activeDotStyle={landingStyleSheet.dots} autoplay={true} autoplayTimeout={3.1}>
                    {this.images.map((image, index) => <CarouselSlide image={image}
                                                                      text={image.text}
                                                                      key={index}/>)}
                </Swiper>
                <TouchableNativeFeedback
                    onPress={this.props.unland}
                    background={TouchableNativeFeedback.SelectableBackground()}>
                    <View style={landingStyleSheet.nextBtn}>
                        <Text style={[landingStyleSheet.textShades, landingStyleSheet.nextBtnTxt]}>Skip ></Text>
                    </View>
                </TouchableNativeFeedback>
            </View>
        );
    }
}