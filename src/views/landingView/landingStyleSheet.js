import {StyleSheet} from 'react-native';
import {GlobalSheet} from '../../config';

//dummy styles down there to change
const landingStyleSheet = StyleSheet.create({
    view: {
        flex: 2,
        width: '100%',
        alignSelf: 'center'
        // marginTop: '1%'
        // top: '-5%'
    },
    carouselView: {
        position: 'relative'
    },
    nextBtn: {
        position: 'absolute', // child
        top: 0, // position where you want
        right: 0,
        marginRight: 15,
        marginTop: 10,
        zIndex: 100
    },
    nextBtnTxt: {
        fontSize: 33,
        fontWeight: 'bold',
        color: '#fff'
    },
    skipBtn: {
        position: 'absolute', // child
        bottom: 0, // position where you want
        right: 0,
        marginRight: 15,
        marginBottom: 10,
        zIndex: 99
    },
    skipBtnTxt: {
        fontSize: 33,
        fontWeight: 'bold',
        color: '#fff'
    },
    textShades: {
        textShadowColor: 'rgba(0, 0, 0, 0.95)',
        textShadowOffset: {width: 1, height: 1},
        textShadowRadius: 8
    }, swiper: {
        position: 'absolute',
        zIndex: 95
    },
    dots: {
        height: 12,
        width: 12,
        borderRadius: 6
    }
});

export default landingStyleSheet;