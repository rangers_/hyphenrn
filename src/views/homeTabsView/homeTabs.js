import React, {Component} from 'react';
import {Text, Modal, View, ImageBackground, TouchableNativeFeedback} from 'react-native';
import homeTabsStyleSheet from './homeTabsStyleSheet';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Colors, NOTIFHANDLER, TOGGLER} from '../../config';
import {createMaterialTopTabNavigator, createAppContainer} from "react-navigation";
import PubSub from 'pubsub-js'
import {NavigationService} from '../../services';

import MessagesView from "../messagesView/messagesView";
import TouristsView from "../touristsView/touristsView";
import BusinessView from "../businessesView/businessView";
import CommunitiesView from "../communitiesView/communitiesView";
import {NotificationModel} from "../../models";

const more = (<Icon name="more-vert" size={35} color={Colors.secondary}/>);
const notifications = (<Icon name="notifications" size={35} color={Colors.secondary}/>);
const menu = (<Icon name="menu" size={35} color={Colors.secondary}/>);

// Tabs navigation
const MainDaddy = createMaterialTopTabNavigator(
    {
        messages: MessagesView,
        tourists: TouristsView,
        business: BusinessView,
        communities: CommunitiesView
    }, {
        swipeEnabled: true,
        tabBarPosition: 'top',
        tabBarOptions: {
            upperCaseLabel: false,
            showIcon: true,
            showLabel: false,
            indicatorStyle: {
                opacity: 0
            },
            style: {
                backgroundColor: Colors.primary
            },
            labelStyle: {
                fontSize: 10,
                marginTop: 1
            }, iconStyle: {
                width: 50,
                height: 50,
                marginTop: -10,
                marginBottom: -10
            },
            activeTintColor: Colors.secondary
        }
    }
);

const AppContainerTabs = createAppContainer(MainDaddy);

export default class HomeTabs extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerRight: (
                <View style={homeTabsStyleSheet.buttonsLeft}>
                    <TouchableNativeFeedback
                        onPress={navigation.getParam('openSmallMenu')}
                        background={TouchableNativeFeedback.SelectableBackground()}
                    >
                        <View style={{marginRight: 9}}>
                            <Text style={{textAlign: 'center', justifyContent: 'center'}}>{notifications}</Text>
                        </View>
                    </TouchableNativeFeedback>
                </View>
            ),
            headerLeft: (
                <TouchableNativeFeedback
                    onPress={navigation.getParam('toggleBtn')}
                    background={TouchableNativeFeedback.SelectableBackground()}
                >
                    <View style={{marginLeft: 9}}>
                        <Text style={{textAlign: 'center', justifyContent: 'center'}}>{menu}</Text>
                    </View>
                </TouchableNativeFeedback>
            )
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({openSmallMenu: this.openSmallMenu});
        this.props.navigation.setParams({toggleBtn: this.toggleBtn});
    }

    constructor(props) {
        super(props);

        this.state = {
            modalVisibilty: true
        }
    }

    openSmallMenu = () => {
        PubSub.publish(NOTIFHANDLER, null);
    };

    toggleBtn = () => {
        PubSub.publish(TOGGLER, null);
    };

    render() {
        return <AppContainerTabs ref={navigatorRef => {
                    NavigationService.setNavigator(navigatorRef, NavigationService.navigators.main.name);
                }}/>
    }
}