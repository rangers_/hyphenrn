import React, {Component} from 'react';
import {Text, View, ImageBackground, TouchableNativeFeedback, ScrollView} from 'react-native';
import allReviewsStyleSheet from './allReviewsStyleSheet';
import Icon from "react-native-vector-icons/MaterialIcons";
import NavigationService from "../../services/navigation/navigationService";
import {ReviewItem} from "../../components/reviewItem";
import chooseCommunityStyleSheet from "../chooseCommunityView/chooseCommunityStyleSheet";
import Colors from "../../config/colors";

const daddyNavigator = NavigationService.navigators.daddy;
const left = (<Icon name="arrow-back" size={35} color={Colors.secondary}/>);


export default class AllReviewsView extends Component {

    comments = {
        az: {
            name: 'Saiida Sousou',
            message: 'Take it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby',
            commentShowAll: false,
            time: '12:55 AM'
        }, azd: {
            name: 'Saiida Sousou',
            message: 'Take it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby',
            commentShowAll: false,
            time: '12:55 AM'
        }, azdgd: {
            name: 'Saiida Sousou',
            message: 'Take it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby',
            commentShowAll: false,
            time: '12:55 AM'
        }, addggdz: {
            name: 'Saiida Sousou',
            message: 'Take it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby',
            commentShowAll: false,
            time: '12:55 AM'
        }, azfdd: {
            name: 'Saiida Sousou',
            message: 'Take it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby',
            commentShowAll: false,
            time: '12:55 AM'
        }, adddddz: {
            name: 'Saiida Sousou',
            message: 'Take it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby',
            commentShowAll: false,
            time: '12:55 AM'
        }, azssdfdd: {
            name: 'Saiida Sousou',
            message: 'Take it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby',
            commentShowAll: false,
            time: '12:55 AM'
        }, adffdsddz: {
            name: 'Saiida Sousou',
            message: 'Take it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby',
            commentShowAll: false,
            time: '12:55 AM'
        }, azdd: {
            name: 'Saiida Sousou',
            message: 'Take it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby',
            commentShowAll: false,
            time: '12:55 AM'
        }, adddz: {
            name: 'Saiida Sousou',
            message: 'Take it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby it easy baby',
            commentShowAll: false,
            time: '12:55 AM'
        },
    };

    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (
                <TouchableNativeFeedback
                    onPress={navigation.getParam('goBack')}
                    background={TouchableNativeFeedback.SelectableBackground()}
                >
                    <View style={{marginLeft: 9}}>
                        <Text style={{textAlign: 'center', justifyContent: 'center'}}>{left}</Text>
                    </View>
                </TouchableNativeFeedback>
            ),
            headerTitle: <Text style={chooseCommunityStyleSheet.title}>Reviews</Text>
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({goBack: this.goBackBtn});
    }

    goBackBtn = () => {
        const { navigation } = this.props;
        navigation.goBack();
    };

    commentsKeys;

    constructor(props) {
        super(props);

        this.commentsKeys = Object.keys(this.comments);
        this.state = {array: this.commentsKeys, ...this.comments}
    }

    goToProfile() {
        NavigationService.navigate(daddyNavigator.name, 'persoView');
    }

    render() {
        return (
            <View style={allReviewsStyleSheet.ViewStyle}>
                <ScrollView>
                    {
                        this.commentsKeys.map((content, key) =>
                            <TouchableNativeFeedback key={key}
                                                     onPress={() => {
                                                         let obj = {};
                                                         obj[content] = Object.assign({}, this.state[content], {commentShowAll: !this.state[content].commentShowAll});
                                                         this.setState({...obj})
                                                     }}
                                                     background={TouchableNativeFeedback.SelectableBackground()}>
                                <View>
                                    <ReviewItem name={this.state[content].name} message={this.state[content].message} time={this.state[content].time} commentShowAll={this.state[content].commentShowAll}/>
                                </View>
                            </TouchableNativeFeedback>)
                    }
                </ScrollView>
            </View>
        );
    }
}