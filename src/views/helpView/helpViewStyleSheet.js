import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

//dummy styles down there to change
const helpViewStyleSheet = StyleSheet.create({
    viewStyle:{
        // alignSelf: 'center',
        backgroundColor: '#f3f3f3',
        zIndex:-0.5,
    },


    txt1Style: {
        fontWeight: 'bold',
        fontSize: 45,
        zIndex:2,
        fontFamily: 'sans-serif',

        color: '#000000' ,
        paddingLeft: 35,
        marginTop: 10,



    },
    txtStyle: {
        fontSize: 25,
        zIndex:2,
        fontFamily: 'sans-serif',

        color: '#000000' ,
        paddingLeft: 35,
        marginTop: 10,


    },
    selectable:{
        color:Colors.facebook
    },
    ImageStyle: {
        // width: 25 * GlobalSheet.units.vw,
        // height: 25 * GlobalSheet.units.vw,
        // borderRadius: 30 * GlobalSheet.units.vw,
        paddingLeft:-50,
        marginTop:-100 ,
        alignSelf: 'center'
    }
});

export default helpViewStyleSheet;