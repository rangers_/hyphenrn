import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, TouchableNativeFeedback} from 'react-native';

import {NavigationService} from '../../services';
import Icon from "react-native-vector-icons/MaterialIcons";
import Colors from "../../config/colors";

import PropTypes from "prop-types";
import helpViewStyleSheet from "../helpView/helpViewStyleSheet";
import GlobalSheet from "../../config/globalStyles";
import chooseCommunityStyleSheet from "../chooseCommunityView/chooseCommunityStyleSheet";
const left = (<Icon name="arrow-back" size={35} color={Colors.secondary}/>);


const navigators = NavigationService.navigators;

export default class HelpView extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (
                <TouchableNativeFeedback
                    onPress={navigation.getParam('goBack')}
                    background={TouchableNativeFeedback.SelectableBackground()}
                >
                    <View style={{marginLeft: 9}}>
                        <Text style={{textAlign: 'center', justifyContent: 'center'}}>{left}</Text>
                    </View>
                </TouchableNativeFeedback>
            ),
            headerTitle: <Text style={chooseCommunityStyleSheet.title}>Help</Text>
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({goBack: this.goBackBtn});
    }

    goBackBtn = () => {
        this.props.navigation.goBack()
    };

    constructor(props) {
        super(props);
        if (this.props.height) {
            this.imageStyle = {
                ...helpViewStyleSheet.ImageStyle,
                height: this.props.height,
                width: this.props.height,
                borderRadius: this.props.height / 2
            }
        } else {
            this.imageStyle = {
                ...helpViewStyleSheet.ImageStyle,
                height: 150 * GlobalSheet.units.vw,
                width: 150 * GlobalSheet.units.vw,
                borderRadius: (150 * GlobalSheet.units.vw) / 2
            };
        }

        if (this.props.image) this.imageDummy = this.props.image;
        else this.imageDummy = require('../../../assets/help.jpg');
    }

    render() {

        return (
            <View style={helpViewStyleSheet.viewStyle}>
                <Text style={helpViewStyleSheet.txt1Style}> Help. </Text>
                <Text style={helpViewStyleSheet.txtStyle}>This is a very basic app and easy to anyone to use, So if you need any further help please contact us on Slack : <Text selectable={true} style={helpViewStyleSheet.selectable}>https://fsmrobotech.slack.com/messages/CDHP66Q65/</Text>
                </Text>
                <Image source={this.imageDummy} style={this.imageStyle}/>
            </View>
        );
    }
}
HelpView.propTypes = {
    text: PropTypes.string,
    height:PropTypes.number,
    width:PropTypes.number,
};