import React, {Component} from 'react';
import {Text, View, ImageBackground, TouchableNativeFeedback} from 'react-native';
import carouselSlideStyleSheet from "./carouselSlideStyleSheet";
import PropTypes from "prop-types";

export class CarouselSlide extends Component {

    constructor(props) {
        super(props);
    }

    image = {
        first: require('../../../assets/slide.jpg')
    };

    skippingBtn() {
    }

    render() {
        return (
            <View style={carouselSlideStyleSheet.view}>
                <ImageBackground source={this.props.image.link} style={carouselSlideStyleSheet.carouselImage}>
                    <Text style={[carouselSlideStyleSheet.textShades, carouselSlideStyleSheet.text]}>{this.props.text}</Text>
                </ImageBackground>
            </View>
        );
    }
}

CarouselSlide.propTypes = {
    text: PropTypes.string,
    image: PropTypes.any
};
