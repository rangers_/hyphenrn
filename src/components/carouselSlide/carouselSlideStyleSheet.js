import {StyleSheet} from 'react-native';
import {GlobalSheet} from '../../config';

//dummy styles down there to change
const carouselSlideStyleSheet = StyleSheet.create({
    view: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignSelf: 'center',
        position: 'absolute'
    },
    text: {
        position: 'absolute', // child
        bottom: 0, // position where you want
        left: 0,
        marginLeft: 26,
        marginBottom: 150,
        fontSize: 40,
        fontWeight: 'bold',
        color: '#fff'
    },
    carouselImage: {
        height: '100%',
        width: '100%',
        position: 'relative', // because it's parent
        top: 0,
        left: 0
    },
    textShades: {
        textShadowColor: 'rgba(0, 0, 0, 0.95)',
        textShadowOffset: {width: 1, height: 1},
        textShadowRadius: 8
    }
});

export default carouselSlideStyleSheet;