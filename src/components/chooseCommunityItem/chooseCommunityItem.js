import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import chooseCommunityItemStyleSheet from "./chooseCommunityItemStyleSheet";
import {Colors} from '../../config';

export class ChooseCommunityItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={chooseCommunityItemStyleSheet.view}>
                {
                    (this.props.index % 2 === 0) ?
                        <View style={chooseCommunityItemStyleSheet.itemStyles}>
                            <Text style={[this.props.selected ? {right: '-20%'} : {
                                right: '-20%',
                                opacity: 0.5
                            }, chooseCommunityItemStyleSheet.textStyle]}>
                                {this.props.name}
                            </Text>
                            <Image source={this.props.image} style={[this.props.selected ? {
                                right: '-21%',
                                borderColor: Colors.secondary
                            } : {
                                right: '-21%',
                                borderColor: 'rgba(198, 198, 198, 0.75)'
                            }, chooseCommunityItemStyleSheet.imageStyle]}/>
                        </View> :
                        <View style={chooseCommunityItemStyleSheet.itemStyles}>
                            <Image source={this.props.image} style={[this.props.selected ? {
                                left: '-21%',
                                borderColor: Colors.secondary
                            } : {
                                left: '-21%',
                                borderColor: 'rgba(198, 198, 198, 0.75)'
                            }, chooseCommunityItemStyleSheet.imageStyle]}/>
                            <Text style={[this.props.selected ? {left: '-20%'} : {
                                left: '-20%',
                                opacity: 0.5
                            }, chooseCommunityItemStyleSheet.textStyle]}>
                                {this.props.name}
                            </Text>
                        </View>
                }
            </View>
        );
    }
}
