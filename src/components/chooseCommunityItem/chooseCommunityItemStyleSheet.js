import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

//dummy styles down there to change
const chooseCommunityItemStyleSheet = StyleSheet.create({
    view: {
        flex: 1,
        width: GlobalSheet.widthPercentageToDP(100),
        height: GlobalSheet.heightPercentageToDP(24),
        alignSelf: 'center'
        // paddingTop: GlobalSheet.heightPercentageToDP(20)
    },
    itemStyles: {
        alignItems: 'center',
        justifyContent: 'center',
        width: GlobalSheet.widthPercentageToDP(100),
        // height: GlobalSheet.heightPercentageToDP(40),
        flexDirection: 'row'
    },
    imageStyle: {
        width: GlobalSheet.widthPercentageToDP(65),
        height: GlobalSheet.widthPercentageToDP(65),
        borderRadius: GlobalSheet.widthPercentageToDP(35),
        borderWidth: 11,
        // borderColor: 'rgba(198, 198, 198, 0.75)',
        // borderColor: 'rgba(198, 198, 198, 0.75)',
    },
    textStyle: {
        textAlign: 'center',
        fontSize: GlobalSheet.widthPercentageToDP(6),
        fontWeight: 'bold',
        marginTop: GlobalSheet.heightPercentageToDP(0.5),
        width: GlobalSheet.widthPercentageToDP(50),
        // opacity: 0.5
    }
});

export default chooseCommunityItemStyleSheet;