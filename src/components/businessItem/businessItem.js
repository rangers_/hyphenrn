import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback} from 'react-native';
import businessItemStyleSheet from "./businessItemStyleSheet";
import PropTypes from "prop-types";
import {GlobalSheet} from '../../config';
import {ImageName} from "../imageName";
import Icon from "react-native-vector-icons/MaterialIcons";

const place = (<Icon name="place" size={20} color="#adadad"/>);

export class BusinessItem extends Component {

    descriptionDummy = 'namename';
    nameDummy = 'namename';
    reviewsDummy = '665 reviews';
    locationDummy = '665 reviews';
    imageDummy = require('../../../assets/avatar.png');

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[businessItemStyleSheet.view]}>
                <ImageName text={this.props.name} image={this.props.image} height={GlobalSheet.widthPercentageToDP(25)}/>
                <Text style={[businessItemStyleSheet.description]}>{this.props.description? this.props.description : this.descriptionDummy}</Text>
                <Text style={[businessItemStyleSheet.location]}>
                    <Text>
                        {place}
                    </Text>
                    {this.props.location ? this.props.location : this.locationDummy}
                </Text>
                <Text style={[businessItemStyleSheet.reviews]}>{this.props.reviews? this.props.reviews : this.reviewsDummy}</Text>
            </View>
        );
    }
}

BusinessItem.propTypes = {
    text: PropTypes.string,
    height:PropTypes.number,
    width:PropTypes.number,
   // uri:propTypes.any
};
