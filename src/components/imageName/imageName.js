import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback} from 'react-native';
import imageNameStyleSheet from "./imageNameStyleSheet";
import PropTypes from "prop-types";
import {GlobalSheet} from '../../config';


export class ImageName extends Component {

    textDummy = 'namename';
    imageDummy;
    imageStyle;
    textStyle;

    constructor(props) {
        super(props);
        if (this.props.height) {
            this.imageStyle = {
                ...imageNameStyleSheet.Image,
                height: this.props.height,
                width: this.props.height, //shut up webstorm I know what to do!
                borderRadius: this.props.height / 2
            }
        } else {
            this.imageStyle = {
                ...imageNameStyleSheet.Image,
                height: 25 * GlobalSheet.units.vw,
                width: 25 * GlobalSheet.units.vw,
                borderRadius: (25 * GlobalSheet.units.vw) / 2
            };
        }

        if (this.props.image) this.imageDummy = this.props.image;
        else this.imageDummy = require('../../../assets/avatar.png');

        if (this.props.textFontSize) {
            this.textStyle = {
                ...imageNameStyleSheet.text,
                fontSize: this.props.textFontSize,
                top: this.props.textTop
            }
        } else {
            this.textStyle = {
                ...imageNameStyleSheet.text,
                fontSize: 25,
                top: -100
            }
        }
    }

    render() {
        return (
            <View style={imageNameStyleSheet.view}>
                <Image source={this.imageDummy} style={this.imageStyle}/>
                <Text style={this.textStyle} height={40} width={40}>{this.props.text? this.props.text : this.textDummy}</Text>
            </View>
        );
    }
}

ImageName.propTypes = {
    text: PropTypes.string,
    height:PropTypes.number,
    width:PropTypes.number,
   // uri:propTypes.any
};
