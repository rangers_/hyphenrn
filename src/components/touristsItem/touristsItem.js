import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback} from 'react-native';
import touristsItemStyleSheet from "./touristsItemStyleSheet";
import PropTypes from "prop-types";
import {GlobalSheet} from '../../config';
import {ImageName} from "../imageName";
import Colors from "../../config/colors";
import Icon from "react-native-vector-icons/MaterialIcons";

const place = (<Icon name="place" size={20} color="#adadad"/>);

export class TouristsItem extends Component {

    textDummy = 'a bio here';
    locationDummy = 'Monastir city';
    imageDummy = require('../../../assets/avatar.png');

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[(this.props.new ? {backgroundColor: Colors.primary1} : {}), touristsItemStyleSheet.view]}>
                <ImageName text={this.props.name}
                           image={this.props.image}
                           height={GlobalSheet.widthPercentageToDP(25)}/>
                <Text style={[touristsItemStyleSheet.bio]}>{this.props.bio ? this.props.bio : this.textDummy}</Text>
                <Text style={[touristsItemStyleSheet.location]}>
                    <Text>
                        {place}
                    </Text>
                    {this.props.location ? this.props.location : this.locationDummy}
                </Text>
            </View>
        );
    }
}

TouristsItem.propTypes = {
    text: PropTypes.string,
    height: PropTypes.number,
    width: PropTypes.number,
    // uri:propTypes.any
};
