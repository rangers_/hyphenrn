import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

const communityNewMessageStyleSheet = StyleSheet.create({
    name: {
        marginLeft: 100,
        marginTop: -55,
        fontSize: 20,
    },
    time: {
        marginLeft: 280,
        marginBottom: 10
    },
    view: {
        backgroundColor: Colors.primary
    }
});
export default communityNewMessageStyleSheet;