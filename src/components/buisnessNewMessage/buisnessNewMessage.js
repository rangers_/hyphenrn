import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback} from 'react-native';
import buisnessNewMessageStyleSheet from "./buisnessNewMessageStyleSheet";
import PropTypes from "prop-types";
import imageNameStyleSheet from "../imageName/imageNameStyleSheet";
import GlobalSheet from "../../config/globalStyles";

export default class BuisnessNewMessage extends Component {
    textDummy = 'has sent you a new message ';
    nameDummy = 'La Reserve \t';
    timeDummy = '10:22 AM';
    imageDummy;

    constructor(props) {
        super(props);
        if (this.props.height) {
            this.imageStyle = {
                ...imageNameStyleSheet.Image,
                height: this.props.height,
                width: this.props.height, //shut up webstorm I know what to do!
                borderRadius: this.props.height / 2
            }
        } else {
            this.imageStyle = {
                ...imageNameStyleSheet.Image,
                height: 15 * GlobalSheet.units.vw,
                width: 15 * GlobalSheet.units.vw,
                borderRadius: (15 * GlobalSheet.units.vw) / 2
            };
        }

        if (this.props.image) this.imageDummy = this.props.image;
        else this.imageDummy = require('../../../assets/avatar.png');

        if (this.props.textFontSize) {
            this.textStyle = {
                ...imageNameStyleSheet.text,
                fontSize: this.props.textFontSize,
                top: this.props.textTop
            }
        } else {
            this.textStyle = {
                ...imageNameStyleSheet.text,
                fontSize: 25,
                top: -100
            }
        }
    }

    render() {
        return (
            <View style={[buisnessNewMessageStyleSheet.view]}>
                <Image source={this.imageDummy} style={this.imageStyle}></Image>
                <Text style={[buisnessNewMessageStyleSheet.name]}><Text
                    style={{fontWeight: 'bold'}}>{this.props.name ? this.props.name : this.nameDummy}</Text><Text
                    style={[buisnessNewMessageStyleSheet.message]}
                    numberOfLines={2}>{this.props.message ? this.props.message : this.textDummy}</Text></Text>
                <Text
                    style={[buisnessNewMessageStyleSheet.time]}>{this.props.time ? this.props.time : this.timeDummy}</Text>
            </View>
        );
    }
};
BuisnessNewMessage.propTypes = {
    text: PropTypes.string,
    height: PropTypes.number,
    width: PropTypes.number,
    // uri:propTypes.any
};