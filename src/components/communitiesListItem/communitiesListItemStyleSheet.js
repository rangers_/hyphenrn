import {StyleSheet} from 'react-native';
import {GlobalSheet,Colors} from '../../config';

//dummy styles down there to change
const communitiesListItemStyleSheet = StyleSheet.create({
    view: {
        width: GlobalSheet.widthPercentageToDP(100),
        height: GlobalSheet.heightPercentageToDP(18)
    },
    text: {
        bottom: 0, // position where you want
        left: 0,
        fontSize: 10,
        fontWeight: 'bold',
        color: 'black',
        marginLeft: 130,
        marginTop: -75,
       // width:'100%'
    },
    Image: {
        width: 25* GlobalSheet.units.vw,
        height: 25 * GlobalSheet.units.vw,
        borderRadius: 30*GlobalSheet.units.vw,
        marginLeft: 20,
        marginTop: 20,
        borderColor: Colors.secondary,
        borderWidth: 2
    },
    bio: {
        fontSize: 18,
        color: '#404040',
        left: 145,
        top: -140,
        // width:'100%'
    },

    membre:{
        fontSize: 20,
        color: '#909090',
        fontWeight: 'bold',
        left: 290,
        top: -100
        // width:'100%'
    }
});

export default communitiesListItemStyleSheet;