import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback} from 'react-native';
import communitiesListItemStyleSheet from "./communitiesListItemStyleSheet";
import PropTypes from "prop-types";
import {GlobalSheet} from '../../config';
import {ImageName} from "../imageName";
import Colors from "../../config/colors";
import Icon from "react-native-vector-icons/MaterialIcons";
import messageItemStyleSheet from "../messageItem/messageItemStyleSheet";
import PublicProfileBuisnViewStyleSheet from "../../views/publicProfileBusinView/PublicProfileBuisnViewStyleSheet";
import {ScrollView} from "../../views/publicProfileBusinView/PublicProfileBuisnView";



export class CommunitiesListItem extends Component {


    membreDummy="450 membre";

    constructor(props) {
        super(props);
    }

    CameraIcon() {

    }

    render() {
        return (
            <View style={[(this.props.new ? {backgroundColor: Colors.primary1} : {}), communitiesListItemStyleSheet.view]}>
                <ImageName textTop={-80} textFontSize={22} text={this.props.name} image={this.props.image}
                           height={GlobalSheet.widthPercentageToDP(25)}/>
                <Text style={[communitiesListItemStyleSheet.membre]}>{this.props.membre? this.props.membreDummy : this.membreDummy}</Text>



            </View>

        );
    }
}

CommunitiesListItem.propTypes = {
    text: PropTypes.string,
membre:PropTypes.any,
    height: PropTypes.number,
    width: PropTypes.number,
    // uri:propTypes.any
};
