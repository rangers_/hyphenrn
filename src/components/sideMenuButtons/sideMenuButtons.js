import React, {Component} from 'react';
import {Text, View, ImageBackground, TouchableOpacity} from 'react-native';
import sideMenuButtonsStyleSheet from './sideMenuButtonsStyleSheet';
import {NavigationService} from '../../services';
import PubSub from 'pubsub-js'
import {TOGGLER} from "../../config";

export default class SideMenuButtons extends Component {

    constructor(props) {
        super(props);
    }

    _onPressButton(daddy, route) {
        NavigationService.navigate(daddy, route);
        PubSub.publish(TOGGLER, null);
    }

    render() {
        return (
            <View>
                {this.props.buttonsList.map((content, key) =>
                    <TouchableOpacity key={key} onPress={() => this._onPressButton(this.props.daddy, content.route)}>
                        <Text style={sideMenuButtonsStyleSheet.buttonStyle}>{content.name}</Text>
                    </TouchableOpacity>
                )}
            </View>
        );
    }
}