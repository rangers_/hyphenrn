import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

//dummy styles down there to change
const sideMenuButtonsStyleSheet = StyleSheet.create({
    buttonStyle: {
        fontWeight: 'bold',
        fontSize: GlobalSheet.widthPercentageToDP(6),
        marginTop: 3
    }
});

export default sideMenuButtonsStyleSheet;