import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback} from 'react-native';
import reviewItemStyleSheet from "./reviewItemStyleSheet";
import PropTypes from "prop-types";
import {GlobalSheet} from '../../config';
import {ImageName} from "../imageName";
import Colors from "../../config/colors";

export class ReviewItem extends Component {

    textDummy = 'Lorem Ipsum has been the\n industry\'s standard dummy\n text ever since the 1500s text ever since the 1500s text ever since the 1500s ';
    nameDummy = 'Samuel Jackson';
    timeDummy = '10:22 AM';

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[reviewItemStyleSheet.view]}>
                <Text style={[reviewItemStyleSheet.name]}>{this.props.name? this.props.name : this.nameDummy}</Text>
                {
                    this.props.commentShowAll ? <Text style={[reviewItemStyleSheet.message]}>{this.props.message? this.props.message : this.textDummy}</Text> :
                        <Text style={[reviewItemStyleSheet.message]} numberOfLines={3}>{this.props.message? this.props.message : this.textDummy}</Text>
                }
                <Text style={[reviewItemStyleSheet.time]}>{this.props.time? this.props.time : this.timeDummy}</Text>
            </View>
        );
    }
}

ReviewItem.propTypes = {
    text: PropTypes.string,
    height:PropTypes.number,
    width:PropTypes.number,
   // uri:propTypes.any
};
