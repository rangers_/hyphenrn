import {StyleSheet} from 'react-native';
import {GlobalSheet,Colors} from '../../config';

//dummy styles down there to change
const reviewItemStyleSheet = StyleSheet.create({
    view: {
        width: GlobalSheet.widthPercentageToDP(100),
        height: GlobalSheet.heightPercentageToDP(18)
    },
    text: {
        bottom: 0, // position where you want
        left: 0,
        fontSize: 25,
        fontWeight: 'bold',
        color: 'black',
        marginLeft: 130,
        marginTop: -75,
        width:'100%'
    },
    Image: {
        width: 25* GlobalSheet.units.vw,
        height: 25 * GlobalSheet.units.vw,
        borderRadius: 30*GlobalSheet.units.vw,
        marginLeft: 20,
        marginTop: 20,
        borderColor: Colors.secondary,
        borderWidth: 2
    },
    message: {
        fontSize: 20,
        color: 'black',
        left: GlobalSheet.widthPercentageToDP(5),
        width: GlobalSheet.widthPercentageToDP(90),
        opacity: 0.7
    },
    time: {
        fontSize: 20,
        color: '#909090',
        fontWeight: 'bold',
        left: 290
        // width:'100%'
    }, name: {
        fontSize: 29,
        fontWeight: 'bold',
        left: GlobalSheet.widthPercentageToDP(4),
        color: '#999999'
    }
});

export default reviewItemStyleSheet;