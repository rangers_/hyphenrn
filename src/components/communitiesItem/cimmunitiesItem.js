import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback} from 'react-native';
import communitiesItemStyleSheet from "./communitiesItemStyleSheet";
import PropTypes from "prop-types";
import {GlobalSheet} from '../../config';
import {ImageName} from "../imageName";
import Colors from "../../config/colors";

export class CimmunitiesItem extends Component {

    textDummy = 'namename';
    timeDummy = '10:22 AM';
    imageDummy = require('../../../assets/avatar.png');

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[(this.props.new ? {backgroundColor: Colors.primary1} : {}), communitiesItemStyleSheet.view]}>
                <ImageName text={this.props.name} image={this.props.image} height={GlobalSheet.widthPercentageToDP(25)}/>
                <Text style={[communitiesItemStyleSheet.message]}>{this.props.message? this.props.message : this.textDummy}</Text>
                <Text style={[communitiesItemStyleSheet.time]}>{this.props.time? this.props.time : this.timeDummy}</Text>
            </View>
        );
    }
}

CimmunitiesItem.propTypes = {
    text: PropTypes.string,
    height:PropTypes.number,
    width:PropTypes.number,
   // uri:propTypes.any
};
