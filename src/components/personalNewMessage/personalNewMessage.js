import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback} from 'react-native';
import personalNewMessageStyleSheet from "./personalNewMessageStyleSheet";
import PropTypes from "prop-types";
import imageNameStyleSheet from "../imageName/imageNameStyleSheet";
import GlobalSheet from "../../config/globalStyles";

export default class PersonalNewMessage extends Component{
    textDummy = 'has sent you a new message ';
    nameDummy = 'Samuel Jackson \t';
    timeDummy = '10:22 AM';
    imageDummy ;
    constructor(props) {
        super(props);
        if (this.props.height) {
            this.imageStyle = {
                ...imageNameStyleSheet.Image,
                height: this.props.height,
                width: this.props.height, //shut up webstorm I know what to do!
                borderRadius: this.props.height / 2
            }
        } else {
            this.imageStyle = {
                ...imageNameStyleSheet.Image,
                height: 15 * GlobalSheet.units.vw,
                width: 15 * GlobalSheet.units.vw,
                borderRadius: (15 * GlobalSheet.units.vw) / 2
            };
        }

        if (this.props.image) this.imageDummy = this.props.image;
        else this.imageDummy = require('../../../assets/avatar.png');

        if (this.props.textFontSize) {
            this.textStyle = {
                ...imageNameStyleSheet.text,
                fontSize: this.props.textFontSize,
                top: this.props.textTop
            }
        } else {
            this.textStyle = {
                ...imageNameStyleSheet.text,
                fontSize: 25,
                top: -100
            }
        }
    }
    render() {
        return(
            <View style={[personalNewMessageStyleSheet.view]}>
                <Image source={this.imageDummy} style={this.imageStyle}></Image>
                <Text style={[personalNewMessageStyleSheet.name]}><Text style={{fontWeight: 'bold'}}>{this.props.name? this.props.name : this.nameDummy}</Text><Text style={[personalNewMessageStyleSheet.message]} numberOfLines={2}>{this.props.message? this.props.message : this.textDummy}</Text></Text>
                <Text style={[personalNewMessageStyleSheet.time]}>{this.props.time? this.props.time : this.timeDummy}</Text>
            </View>
        );
    }
};
PersonalNewMessage.propTypes ={
    text: PropTypes.string,
    height:PropTypes.number,
    width:PropTypes.number,
    // uri:propTypes.any
}