import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback} from 'react-native';
import messageItemStyleSheet from "./messageItemStyleSheet";
import PropTypes from "prop-types";
import {GlobalSheet} from '../../config';
import {ImageName} from "../imageName";
import Colors from "../../config/colors";

export class MessageItem extends Component {

    textDummy = 'namename';
    timeDummy = '10:22 AM';

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[(this.props.new ? {backgroundColor: Colors.primary1} : {}), messageItemStyleSheet.view]}>
                <ImageName text={this.props.name} image={this.props.image} height={GlobalSheet.widthPercentageToDP(25)}/>
                <Text style={[messageItemStyleSheet.message]}>{this.props.message? this.props.message : this.textDummy}</Text>
                <Text style={[messageItemStyleSheet.time]}>{this.props.time? this.props.time : this.timeDummy}</Text>
            </View>
        );
    }
}

MessageItem.propTypes = {
    text: PropTypes.string,
    height:PropTypes.number,
    width:PropTypes.number,
   // uri:propTypes.any
};
