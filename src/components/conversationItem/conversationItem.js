import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback} from 'react-native';
import conversationItem from "./conversationItemStyleSheet";
import PropTypes from "prop-types";
import {GlobalSheet} from '../../config';
import {ImageName} from "../imageName";
import Colors from "../../config/colors";

export class ConversationItem extends Component {

    textDummy = 'namename';
    timeDummy = '10:22 AM';

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[(this.props.new ? {backgroundColor: Colors.primary1} : {}), conversationItem.view]}>
                <ImageName text={this.props.name} image={this.props.image} height={GlobalSheet.widthPercentageToDP(25)}/>
                <Text style={[conversationItem.message]}>{this.props.message? this.props.message : this.textDummy}</Text>
                <Text style={[conversationItem.time]}>{this.props.time? this.props.time : this.timeDummy}</Text>
            </View>
        );
    }
}

ConversationItem.propTypes = {
    text: PropTypes.string,
    height:PropTypes.number,
    width:PropTypes.number,
   // uri:propTypes.any
};
