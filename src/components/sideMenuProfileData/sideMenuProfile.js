import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import sideMenuProfileStyleSheet from './sideMenuProfileStyleSheet';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Colors from "../../config/colors";

const place = (<Icon name="place" size={25} color={Colors.secondary}/>);

export default class SideMenuProfile extends Component {

    image = require('../../../assets/personalProfileImg.jpg');
    name = 'Mohsen Moh';
    adress = 'Monastir, Street n°2';

    _onPressName() {

    }
    _onPressAdress() {

    }

    render() {
        return (
            <View style={sideMenuProfileStyleSheet.ViewStyle}>
                <Image source={this.image} style={sideMenuProfileStyleSheet.imageStyle}/>
                <TouchableOpacity onPress={this._onPressName}>
                    <Text style={sideMenuProfileStyleSheet.textStyle}>{this.name}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this._onPressAdress}>
                    <Text style={sideMenuProfileStyleSheet.placeStyle}>
                        <Text>{place}</Text>
                        {this.adress}
                    </Text>
                </TouchableOpacity>

            </View>
        );
    }
}