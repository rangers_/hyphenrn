import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

//dummy styles down there to change
const sideMenuProfileStyleSheet = StyleSheet.create({
    ViewStyle: {
        flex: 1,
        height: '100%',
        width: '100%',
        backgroundColor: Colors.primary
    },
    buttonsLeft: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    imageStyle: {
        width: 40 * GlobalSheet.units.vw,
        height: 40 * GlobalSheet.units.vw,
        borderRadius: 20 * GlobalSheet.units.vw,
        borderColor: Colors.secondary,
        borderWidth: 3
    },
    textStyle: {
        fontWeight: 'bold',
        fontSize: 9 * GlobalSheet.units.vw,
    },
    placeStyle: {
        fontSize: 4.5 * GlobalSheet.units.vw,
        marginTop: 52
    }
});

export default sideMenuProfileStyleSheet;