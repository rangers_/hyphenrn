import React, {Component} from 'react';
import {Text, Modal, View, TouchableOpacity, TouchableWithoutFeedback, ScrollView} from 'react-native';
import notificationModelStyleSheet from './notificationModelStyleSheet';
import PropTypes from "prop-types";
import PersonalNewMessage from "../../components/buisnessNewMessage/buisnessNewMessage";
import CommunityNewMessage from "../../components/communityNewMessage/communityNewMessage";
import {BuisnessNewMessage} from "../../components/buisnessNewMessage";

export default class NotificationModel extends Component {
    render() {
        return (

            <Modal animationType={"slide"} visible={this.props.modalVisibilty} presentationStyle={"overFullScreen"}
                   transparent={true} onRequestClose={() => {
            }}>
                <TouchableWithoutFeedback onPress={this.props.updateVisibilty}>
                    <View style={notificationModelStyleSheet.notificationBack}>
                        <View style={notificationModelStyleSheet.notificationPopup}>
                            <ScrollView>
                                <PersonalNewMessage/>
                                <CommunityNewMessage/>
                                <BuisnessNewMessage/>
                                <BuisnessNewMessage/>
                                <BuisnessNewMessage/>
                                <PersonalNewMessage/>
                                <PersonalNewMessage/>
                            </ScrollView>
                            <View>
                                <TouchableOpacity
                                    onPress={this.props.updateVisibilty} style={notificationModelStyleSheet.buttonSeen}
                                >
                                    <Text style={notificationModelStyleSheet.buttonSeenText}>Mark All As Seen</Text>
                                </TouchableOpacity></View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        );
    }

};