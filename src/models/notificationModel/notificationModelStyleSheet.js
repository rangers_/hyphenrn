import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';


const notificationModelStyleSheet = StyleSheet.create({
    buttonSeen: {
        width: '100%',
        backgroundColor: Colors.secondary,
        height: 60,
        //borderRadius: 15,
        //borderWidth: 3,
        borderColor: 'transparent',
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15

    },
    buttonSeenText: {
        color: '#fff',
        fontSize: 27,
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: 10
    },
    notificationPopup:{
        height:'50%',
        borderRadius:20,
        borderWidth: 3,
        marginTop: 60,
        marginLeft: 35,
        marginRight: 20 ,
        borderColor:'transparent',
        elevation: 10,
        shadowColor: Colors.grey5,
        backgroundColor: Colors.primary,
        zIndex: 100
    },
    notificationBack: {
        width: GlobalSheet.widthPercentageToDP(100),
        height: GlobalSheet.heightPercentageToDP(100)
    }

});

export default notificationModelStyleSheet;