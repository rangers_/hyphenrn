import { NavigationActions } from 'react-navigation';

const navigators = {
    main: {name: 'mainNavigator'},
    daddy: {name: 'daddyNavigator'},
    signUp: {name: 'signUpNavigator'},
    signing: {name: 'signingNavigator'},
    discover: {name: 'discoverNavigator'},
    profile: {name: 'profileNavigator'},
    notifications: {name: 'notificationsNavigator'},
    messages: {name: 'messagesNavigator'},
    messagesNav: {name: 'messagesNavNavigator'}
};
let _navigator = {};
let _navParams = {};

function setNavigator(navigatorRef, name: string) {
    _navigator[name] = navigatorRef;
}

function setNavParams(navigatorName, params) {
    _navParams[navigatorName] = params;
}

function navigate(navigatorName, routeName, params) {
    setNavParams(navigatorName, params);
    console.log(_navParams[navigatorName]);
    _navigator[navigatorName].dispatch(
        NavigationActions.navigate({
            routeName,
            params,
        })
    );
}

function getNavParams(navigatorName) {
    console.log(_navParams[navigatorName]);
    return _navParams[navigatorName];
}

export default {
    navigate,
    setNavigator,
    navigators,
    getNavParams,
    setNavParams
};