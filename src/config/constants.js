const TOGGLER = 'TOGGLER';
const NOTIFHANDLER = 'NOTIFHANDLER';
const COMMUNITIES = {
    "food": {
        name: "Food community",
        image: require('../../assets/food.jpg'),
        selected: false
    }, "destinations": {
        name: "Destinations community",
        image: require('../../assets/dest.jpg'),
        selected: false
    }, "accommodations": {
        name: "Accommodations community",
        image: require('../../assets/acco.jpg'),
        selected: false
    }, "shopping": {
        name: "Shopping community",
        image: require('../../assets/shopping.jpg'),
        selected: false
    }, "sports": {
        name: "Sports community",
        image: require('../../assets/sport.jpg'),
        selected: false
    }, "bars": {
        name: "Bars community",
        image: require('../../assets/bars.jpg'),
        selected: false
    }, "coffee_shops": {
        name: "Coffee shops community",
        image: require('../../assets/coffe.jpg'),
        selected: false
    }, "entertainment": {
        name: "Entertainments community",
        image: require('../../assets/enter.jpg'),
        selected: false
    },
};

export {
    TOGGLER,
    COMMUNITIES,
    NOTIFHANDLER
};